# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### `npm run analyze`

To analyze the bundle run the production build then run the analyze script. This helps you understand where code bloat is coming from.

## Important Dependencies

- [axios](https://github.com/axios/axios) - http requests
- [moment](https://momentjs.com/) - dates handler
- [lodash](https://lodash.com/docs/4.17.15) - utilities
- [husky](https://github.com/typicode/husky) - git hooks made easy!
- [react-toastify](https://github.com/fkhadra/react-toastify) - toast notifications
- [react-helmet-async](https://github.com/staylor/react-helmet-async) - reusable React component will manage all of your changes to the document head
- [react-lazy-load-image-component](https://github.com/Aljullu/react-lazy-load-image-component) - reusable React component to lazy load images and other components/elements

## Git hooks

The project use Husky as [Git Hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) handler. The Hooks created for this template are:   
- pre-commit: will prettify the files inside src folder, based on .prettierrc file rules. 
- pre-push: will make sure the project builds without errors and all the tests pass. 

## General info

The template uses airbnb linter rules, with some exceptions (.eslintrc for more info).  
There is a Dockerfile ready to use if you want to use deploy the project as a docker image (Dockerfile and nginx.conf for more info).  
You can change the settings of the project based on your preference/requirements.  
If you want to propose improvements get in touch with Alessandro or Geo.