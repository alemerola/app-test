# Step 1
# Install node modules, build the react app
FROM node:10-alpine as build-step
RUN mkdir /app
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
RUN npm ci
COPY . /app
RUN npm run build

# Step 2
# Run a custom nginx configuration
FROM nginx:1.17.1-alpine
COPY --from=build-step /app/build /var/www
COPY --from=build-step /app/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80 443
ENTRYPOINT ["nginx","-g","daemon off;"]