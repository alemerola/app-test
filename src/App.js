import React from 'react';
import Routing from './Routing';
import { NotificationContainer } from './components/ToastNotification';

const App = () => {
  return (
    <div dir="rlr">
      <NotificationContainer />
      <Routing />
    </div>
  );
};

export default App;
