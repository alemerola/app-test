import React from 'react';
import { Navigate } from 'react-router-dom';
import MainLayout from './layouts/MainLayout';
import NotFoundView from './views/NotFound';
// import HomePage from './views/HomePage';
import PassportCrop from './views/PassportCrop';

function getRoutes() {
  const routes = [
    {
      path: '/',
      element: <MainLayout />,
      children: [
        { path: '/', element: <PassportCrop /> },
        { path: '404', element: <NotFoundView /> },
        { path: '*', element: <Navigate to="/404" /> },
      ],
    },
  ];
  return routes;
}

export default getRoutes;
