import http from './http';

const Api = {
  // samplePost: (body) => http.post(`/sample/url`, body) ,
  // sampleGet: (qs1, qs2) => http.get('/sample/url', {params: {qs1, qs2}}),
  test: () => http.get('https://jsonplaceholder.typicode.com/todos/1'),
};

export default Api;
