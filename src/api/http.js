import axios from 'axios';

const http = (config) => {
  const apiConfig = {
    baseURL: process.env.REACT_APP_API_BASE_URL,
    timeout: 10000,
  };

  const handleSuccess = (response) => response.data;

  const handleError = (error) => {
    if (error.response) {
      return {
        errorMessage: error.response.data.message,
        errorStatus: error.response.status,
      };
    }
    return {
      errorMessage: error,
    };
  };

  const apiClient = axios.create(config || apiConfig);

  apiClient.interceptors.response.use(handleSuccess, handleError);

  return apiClient;
};

export default http({});

export const customHttp = http;
