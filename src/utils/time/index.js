import moment from 'moment';

export const today = () => moment().toDate();
