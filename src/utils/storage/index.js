const projectName = process.env.REAC_APP_PROJECT_NAME || 'ReactApp';
const storageKey = (key) => `${projectName}_${key}`;

const storageOperations = (storage) => {
  return {
    get: (key) => JSON.parse(storage.getItem(storageKey(key))),
    set: (key, value) => storage.setItem(storageKey(key), JSON.stringify(value)),
    remove: (key) => storage.removeItem(storageKey(key)),
    clear: () => storage.clear(),
  };
};

export const LocalStorage = storageOperations(localStorage);
export const SessionStorage = storageOperations(sessionStorage);
