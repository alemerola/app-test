import { groupBy as _groupBy } from 'lodash';

export const groupBy = (array, iteratee) => _groupBy(array, iteratee);
