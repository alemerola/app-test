import React from 'react';
import { useRoutes } from 'react-router-dom';
import getRoutes from './routes';

export default () => {
  const routing = useRoutes(getRoutes());
  return <>{routing}</>;
};
