import React, { useEffect, useState } from 'react';
import Page from '../../components/Page';
import FileInput from '../../components/FileInput';
import ImageCrop from '../../components/ImageCropper';
import INPUT_FILE from '../../constants/inputFile';

const PassportCrop = () => {
  const [fileUploaded, setUploadFile] = useState();
  const [croppedImage, setCroppedImage] = useState();
  console.log(fileUploaded);

  useEffect(() => {
    console.log(croppedImage);
  }, [croppedImage]);

  return (
    <Page title="Crop">
      <FileInput accept={INPUT_FILE.ACCEPT.IMAGES} setUploadedFile={setUploadFile} />
      {fileUploaded && <ImageCrop imageSrc={fileUploaded} onCropComplete={setCroppedImage} />}
    </Page>
  );
};

export default PassportCrop;
