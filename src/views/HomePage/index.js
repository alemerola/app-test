import React from 'react';
import Page from '../../components/Page';

const HomePage = () => {
  return (
    <Page title="Home">
      <div>Home Page</div>
    </Page>
  );
};

export default HomePage;
