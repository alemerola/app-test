import React from 'react';
import Page from '../../components/Page';

const NotFoundView = () => {
  return (
    <Page title="404">
      <div>404 - NOT FOUND</div>
    </Page>
  );
};

export default NotFoundView;
