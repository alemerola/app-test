import React, { useState, useCallback, useRef, useEffect } from 'react';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import PropTypes from 'prop-types';

/**
 *
 * @param imageSrc The source of the image that we want to crop, it can be a url or a base64.
 * @param onCropComplete Called once the cropping is completed and the new Image is created.
 *
 * onCropComplete function will be called passing the cropped base64 image data as argument
 *
 * @returns {JSX.Element}
 */
const ImageCrop = ({ imageSrc, onCropComplete }) => {
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  // crop data looks like
  // {
  //    width,
  //    height,
  //    x,
  //    y
  // }
  const [crop, setCrop] = useState();
  const [completedCrop, setCompletedCrop] = useState(null);

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const pixelRatio = window.devicePixelRatio;

    const canvas = previewCanvasRef.current;
    const ctx = canvas.getContext('2d');

    canvas.width = completedCrop.width * pixelRatio;
    canvas.height = completedCrop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';
    // Based on the data of completedCrop we draw a new image on a hidden canvas
    ctx.drawImage(
      image,
      completedCrop.x * scaleX,
      completedCrop.y * scaleY,
      completedCrop.width * scaleX,
      completedCrop.height * scaleY,
      0,
      0,
      completedCrop.width,
      completedCrop.height,
    );

    // The canvas data are converted in base64 and send to the father component
    onCropComplete(canvas.toDataURL());
  }, [completedCrop]);

  return (
    <div>
      <ReactCrop
        src={imageSrc}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={(c) => setCrop(c)}
        onComplete={(c) => setCompletedCrop(c)}
      />
      <div hidden>
        <canvas id="c" ref={previewCanvasRef} />
      </div>
    </div>
  );
};

ImageCrop.propTypes = {
  imageSrc: PropTypes.string.isRequired,
  onCropComplete: PropTypes.func.isRequired,
};

export default ImageCrop;
