import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import PropTypes from 'prop-types';
import 'react-toastify/dist/ReactToastify.css';
import './toastify.css';

/*
  Utility to generate the Toast notifications. Update styles in the toastify.css file.
*/

const options = {
  autoClose: 4500,
  draggable: false,
  position: 'top-center',
  hideProgressBar: false,
};
const optionsSmall = { ...options, closeButton: false, className: 'small-toast' };
const optionsBig = { ...options, closeButton: false, className: 'big-toast' };

const ToastComponent = ({ message }) => {
  return <span className="toast-message">{message}</span>;
};

const notifications = {
  warning: (message) => toast.warning(<ToastComponent message={message} />, options),
  success: (message) => toast.success(<ToastComponent message={message} />, options),
  error: (message) => toast.error(<ToastComponent message={message} />, options),
  warningSmall: (message) => toast.warning(message, optionsSmall),
  successSmall: (message) => toast.success(message, optionsSmall),
  errorSmall: (message) => toast.error(message, optionsSmall),
  warningBig: (message) => toast.warning(<ToastComponent message={message} />, optionsBig),
  successBig: (message) => toast.success(<ToastComponent message={message} />, optionsBig),
  errorBig: (message) => toast.error(<ToastComponent message={message} />, optionsBig),
};

ToastComponent.propTypes = {
  message: PropTypes.string.isRequired,
};

export { ToastContainer as NotificationContainer, notifications };
