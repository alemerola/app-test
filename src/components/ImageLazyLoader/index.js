import React from 'react';
import {
  LazyLoadImage,
  trackWindowScroll as _trackWindowScroll,
} from 'react-lazy-load-image-component';
import PropTypes from 'prop-types';
import 'react-lazy-load-image-component/src/effects/blur.css';
import 'react-lazy-load-image-component/src/effects/black-and-white.css';
import 'react-lazy-load-image-component/src/effects/opacity.css';

const _EFFECT_TYPES = {
  BLUR: 'blur',
  OPACITY: 'opacity',
  BLACK_AND_WHITE: 'black-and-white',
};

// All img attributes are available
const LazyLoadedImage = ({ src, effect, placeholderSrc, ...rest }) => (
  <div>
    <LazyLoadImage effect={effect} src={src} placeholderSrc={placeholderSrc} {...rest} />
  </div>
);

LazyLoadedImage.propTypes = {
  src: PropTypes.string.isRequired,
  effect: PropTypes.oneOf([...Object.values(_EFFECT_TYPES), '']),
  placeholderSrc: (props) => {
    if (
      props.effect &&
      (props.effect === _EFFECT_TYPES.BLUR || props.effect === _EFFECT_TYPES.BLACK_AND_WHITE) &&
      props.placeholderSrc === ''
    ) {
      throw new Error(
        'Please provide a placeholderSrc function if you choose blur or black-and-white effect!',
      );
    }
  },
};

LazyLoadedImage.defaultProps = {
  effect: '',
  placeholderSrc: '',
};

export default LazyLoadedImage;
export const EFFECT_TYPES = _EFFECT_TYPES;
export const trackWindowScroll = _trackWindowScroll;
