import React from 'react';
import PropTypes from 'prop-types';

const FileInput = ({ accept, setUploadedFile }) => {
  const onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUploadedFile(reader.result));
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  return (
    <div>
      <input type="file" accept={accept} onChange={onSelectFile} />
    </div>
  );
};

FileInput.propTypes = {
  setUploadedFile: PropTypes.func.isRequired,
  // treat this prop as the HTML attribute and use one of the allowed strings ex: image/* or .pdf
  accept: PropTypes.string.isRequired,
};

export default FileInput;
